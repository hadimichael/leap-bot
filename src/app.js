/*
 *
 * @author: @hadi_michael
 * @date: July-August 2013
 *
 */

/* include and setup required packages */
var appName		= 'Leap-Bot',
	express 	= require('express'),
    app     	= express(),
    http 		= require('http'),
	server 		= http.createServer(app), 
	io  		= require('socket.io').listen(server),
	five 		= require('johnny-five'),
    board 		= new five.Board();

/***** HANDLE Uncaught Exceptions *****/
process.on('uncaughtException', function(err) {
  	console.error(err.stack);
});
/***** /HANDLE Uncaught Exceptions *****/

/***** CONFIGURE AND START SERVER *****/

// configure the server
app.configure(function(){
	app.use(express.bodyParser());
	app.use(express.static(__dirname + '/client'));
  	app.use(express.errorHandler({
    	dumpExceptions: true, 
    	showStack: true
	}));
});

// start the server, once the Arduino board is ready
board.on("ready", function() {
	var portNumber = 8080;
	try {
		server.listen(portNumber);
		initHardware();
		console.log('Server is listening on port: ' + portNumber);
	} catch (err) {
		console.error('Server didn\'t start: ' + err);
	}	
});

/***** /CONFIGURE AND START SERVER *****/

/***** INITIALISE HARDWARE *****/

var motorA, motorB;
function initHardware() {
	// Create a new `motor` hardware instances
	var motorA_pins = {
		PWM: 6,
		CW: 4,
		CCW: 7 
	};
	var motorB_pins = {
		PWM: 5,
		CW: 3,
		CCW: 2 
	};

	motorA = new five.Motor({
	    pins: {
	      pwm: motorA_pins.PWM,
	      dir: motorA_pins.CCW,
	      cdir: motorA_pins.CW
	    }
	});

	motorB = new five.Motor({
	    pins: {
	      pwm: motorB_pins.PWM,
	      dir: motorB_pins.CW,
	      cdir: motorB_pins.CCW
	    }
	});

	motorA.start();
	motorB.start();

	//Register Motor Event API
	motorA.on("start", function(err, timestamp) {
		console.log("start", timestamp);
	});

	motorA.on("stop", function(err, timestamp) {
		console.log("stop", timestamp);
	});

	motorA.on("forward", function(err, timestamp) {
		console.log("forward", timestamp);
	});

	motorA.on("reverse", function(err, timestamp) {
		console.log("reverse", timestamp);
	});

}

/***** /INITIALISE HARDWARE *****/

/***** MANAGE SOCKETS *****/
io.set('log level', 2); // set socket.io logging (Log levels: 0 - error / 1 - warn / 2 - info / 3 - debug)
io.sockets.on('connection', function (socket) {

	console.log('New user connected: ' + socket.id);
  	socket.emit('connected', { hello: 'Welcome to the party!' });

  	/* define socket routes */	
	socket.on('drive', function(options) {
		//accepts options.velocity from -255 to 255 (don't need to check the range, this is done inside johnny-five)
		//accepts options.duration in ms
		console.log(options);
		options.left = options.left || 0;
		options.right = options.right || 0;
		options.duration = options.duration || 0; //default to 0 milli second if not specified
		//console.log("USED: " + JSON.stringify(options));

		//	motorA is LEFT
		//	motorB is RIGHT
		if (options.left > 0) {
			motorA.forward(options.left);
		} else if (options.left < 0) {
			motorA.reverse(Math.abs(options.left));
		} else {
			motorA.stop();
		}

		if (options.right > 0) {
			motorB.forward(options.right);
		} else if (options.right < 0) {
			motorB.reverse(Math.abs(options.right));
		} else {
			motorB.stop();
		}

		if (options.duration > 0) {
			board.wait(options.duration, function() { 
				motorA.stop();
				motorB.stop();
			});
		} else if (options.duration == 0) {
			//keep driving
		}
	});

	socket.on('faststop', function() {
		console.log("SOCKET:: CLOSE EMERGENCY".warn);
		process.exit();
    });

	socket.on('debug', function(obj) {

	});

	socket.on('disconnect', function () {
    	console.log('User disconnected - Damn... I thought they liked us!');
  	});

});
/***** /MANAGE SOCKETS ****/