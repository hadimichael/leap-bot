var LEAP_BOT = window.LEAP_BOT || {};

LEAP_BOT.isDebug = true;

LEAP_BOT.leap = (function(){

	var _controller = new Leap.Controller({ enableGestures: true });

	// controller events
	_controller.on('ready', function() {
		console.log("Leap: Ready");
	}).on('connect', function() {
		console.log("Leap: Do connect");
	}).on('disconnect', function() {
		console.log("Leap: Do disconnect");
	}).on('focus', function() {
		console.log("Leap: Focus");
	}).on('blur', function() {
		console.log("Leap: Blur");
	}).on('deviceConnected', function() {
		console.log("Leap: Device Connected");
	}).on('deviceDisconnected', function() {
		console.log("Leap: Device Disconnected");
	});

	return {
		controller: _controller
	}

}());

LEAP_BOT.values = (function(){

	var _prev = {};

	var _updatePrevValues = function(hand) {
		_prev.stabilizedPalmPosition = hand.stabilizedPalmPosition;
		_prev.palmNormal = hand.palmNormal;
	};

	var _roundArray = function(arr) {
		var roundedArr = [];
		for(var i=0,len=arr.length; i<len; i++) {
			roundedArr.push(parseInt(arr[i], 10));
		}

		return roundedArr;
	};

	// main loop function
	LEAP_BOT.leap.controller.loop(function(frame) {
		// only run when a hand is visible
		if(frame.hands.length > 0) {
			$(document).trigger('updateValues.leapbot', [frame.hands[0], _prev]);
			_prev = frame.hands[0];
		}
	});

	return {
		roundArray: _roundArray
	}

}());

LEAP_BOT.socket = (function(){
	var _address = window.location.origin,
		_head = document.getElementsByTagName('head')[0],
    	_script = document.createElement('script');
    
    _script.type = 'text/javascript';
    _script.src = _address + '/socket.io/socket.io.js';

    //functions
    var _emit = function(verb, json){
    	console.log('debug emit without websockets loaded', verb, json);
    };


    _script.onload = function() { 
        var socket = io.connect(_address); 

        socket.on('connected', function (message) {
            console.log('Socket connected', message); //display message
            
            LEAP_BOT.socket.emit = function(verb, json) {
            	socket.emit(verb, json);
            }
        });
    };

    // adding the script tag to the head
    _head.appendChild(_script);

    return {
    	emit: _emit
    }
}());

$(document).ready(function(){

	$(document).on('updateValues.leapbot', function(event, current, previous){
		$('#console').html('stabilized palm: ' + LEAP_BOT.values.roundArray(current.stabilizedPalmPosition) + '<br/>palm normal: ' + current.palmNormal);

		//LEAP_BOT.socket.emit('test', LEAP_BOT.values.roundArray(current.stabilizedPalmPosition));
	});

	var driving = false;
	var speed = 128;
	var speedIncreaseRate = 2;

	/* Setup control of the bot using gest.js */
	document.addEventListener('gest', function(gesture) {
		console.log(gesture);
		if (gesture.up)  {
			LEAP_BOT.socket.emit('drive', {left: speed, right: speed, duration: 1000});
		} else if (gesture.down) {
			LEAP_BOT.socket.emit('drive', {left: -speed, right: -speed, duration: 1000});	
		} else if (gesture.left) {
			LEAP_BOT.socket.emit('drive', {left: speed, right: -speed, duration: 1000});	
		} else if (gesture.right) {
			LEAP_BOT.socket.emit('drive', {left: -speed, right: speed, duration: 1000});	
		};
	}, false);
	/* /Setup control of the bot using gest.js */

	/* Setup control of the bot using keypad */
	kd.run(function () {
	 	kd.tick();
	});

	kd.UP.down(function () {
		if (driving) {speed += speedIncreaseRate;}

		//forwards
		LEAP_BOT.socket.emit('drive', {left: speed, right: speed, duration: 0});		

		driving = true;
	});

	kd.DOWN.down(function () {
		if (driving) {speed += speedIncreaseRate;}

		//backwards
		LEAP_BOT.socket.emit('drive', {left: -speed, right: -speed, duration: 0});		

		driving = true;
	}); 

	kd.LEFT.down(function () {
		if (driving) {speed += speedIncreaseRate;}

		//left
		LEAP_BOT.socket.emit('drive', {left: 0, right: speed, duration: 0});		

		driving = true;
	}); 

	kd.RIGHT.down(function () {
		if (driving) {speed += speedIncreaseRate;}

		//right
		LEAP_BOT.socket.emit('drive', {left: speed, right: 0, duration: 0});		

		driving = true;
	});
	
	var keys = {
		forwards: 38,
		backwards: 40,
		left: 37,
		right: 39
	}

	$(document).keyup(function(event) {
		if (event.keyCode == keys.forwards || event.keyCode == keys.backwards || event.keyCode ==  keys.left || event.keyCode ==  keys.right) {
			//stop
			driving = false;
			speed = 128;
			LEAP_BOT.socket.emit('drive', {left: 0, right: 0, duration: 0});
		}
	});
	/* /Setup control of the bot using keypad */
	
});